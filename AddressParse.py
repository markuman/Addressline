import re

def AddressParse(string):
    street_and_extension = re.split("\d+\w", string)
    if len(street_and_extension) == 1:
        street_and_extension = re.split("\d+", string)

    # rare condition. appears one time in test dataset
    if len(street_and_extension) == 3:
        arr = re.split("No", string)
        street = arr[0].strip()
        house_number = ["No " + arr[1].strip()]

    # covers most addresses in the test dataset
    else:
        street_idx = 0
        # when first regex is an empty string, the addresse is flipped
        if street_and_extension[0] == "":
            street_idx = -1
        # extract street and remove noisy chars (whitespaces and ,)
        street = street_and_extension[street_idx].replace(",","").strip()
        house_number = re.findall("\d+\w", string) or re.findall("\d+", string)

    # check if the house_number has an extension
    house_number_extension = ""
    if len(street_and_extension) == 2 and street_idx == 0:
        house_number_extension = street_and_extension[1].strip()

    return [street, house_number[0] + house_number_extension]
