import unittest
from AddressParse import AddressParse
from enum import IntEnum
class Idx(IntEnum):
    Street = 0
    HouseNr = 1



class testAddressParse(unittest.TestCase):

    def test_simple_addess(self):
        retval = AddressParse("Winterallee 3")
        self.assertEqual(retval[Idx.Street], "Winterallee")
        self.assertEqual(retval[Idx.HouseNr], "3")

    def test_greater_house_number(self):
        retval = AddressParse("Musterstrasse 45")
        self.assertEqual(retval[Idx.Street], "Musterstrasse")
        self.assertEqual(retval[Idx.HouseNr], "45")

    def test_more_greater_house_number(self):
        retval = AddressParse("Blaufeldweg 1238")
        self.assertEqual(retval[Idx.Street], "Blaufeldweg")
        self.assertEqual(retval[Idx.HouseNr], "1238")

    def test_splitted_streetname(self):
        retval = AddressParse("Am Bächle 23")
        self.assertEqual(retval[Idx.Street], "Am Bächle")
        self.assertEqual(retval[Idx.HouseNr], "23")

    def test_splitted_streetname_with_house_number_extension(self):
        retval = AddressParse("Auf der Vogelwiese 11b")
        self.assertEqual(retval[Idx.Street], "Auf der Vogelwiese")
        self.assertEqual(retval[Idx.HouseNr], "11b")

    def test_splitted_streetname_with_house_number_whitespace_extension(self):
        retval = AddressParse("Auf der Vogelwiese 23 b")
        self.assertEqual(retval[Idx.Street], "Auf der Vogelwiese")
        self.assertEqual(retval[Idx.HouseNr], "23b")

    def test_comma_splitted_streetname_with_house_number(self):
        retval = AddressParse("Calle Aduana, 29")
        self.assertEqual(retval[Idx.Street], "Calle Aduana")
        self.assertEqual(retval[Idx.HouseNr], "29")

    def test_flipped_streetname_with_house_number(self):
        retval = AddressParse("4, rue de la revolution")
        self.assertEqual(retval[Idx.Street], "rue de la revolution")
        self.assertEqual(retval[Idx.HouseNr], "4")

    def test_flipped_streetname_with_house_number_without_separator(self):
        retval = AddressParse("200 Broadway Av")
        self.assertEqual(retval[Idx.Street], "Broadway Av")
        self.assertEqual(retval[Idx.HouseNr], "200")

    def test_a_rare_condition(self):
        retval = AddressParse("Calle 39 No 1540")
        self.assertEqual(retval[Idx.Street], "Calle 39")
        self.assertEqual(retval[Idx.HouseNr], "No 1540")

    def test_flip_address_with_housenumber_extension(self):
        retval = AddressParse("8a, Invalidenstr")
        self.assertEqual(retval[Idx.Street], "Invalidenstr")
        self.assertEqual(retval[Idx.HouseNr], "8a")

if __name__ == "__main__":
    unittest.main()
