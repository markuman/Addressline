# Addressline

[![pipeline status](https://gitlab.com/markuman/Addressline/badges/master/pipeline.svg)](https://gitlab.com/markuman/Addressline/commits/master)

```
python3 -m unittest testAddressParse.testAddressParse -v
test_a_rare_condition (testAddressParse.testAddressParse) ... ok
test_comma_splitted_streetname_with_house_number (testAddressParse.testAddressParse) ... ok
test_flip_address_with_housenumber_extension (testAddressParse.testAddressParse) ... ok
test_flipped_streetname_with_house_number (testAddressParse.testAddressParse) ... ok
test_flipped_streetname_with_house_number_without_separator (testAddressParse.testAddressParse) ... ok
test_greater_house_number (testAddressParse.testAddressParse) ... ok
test_more_greater_house_number (testAddressParse.testAddressParse) ... ok
test_simple_addess (testAddressParse.testAddressParse) ... ok
test_splitted_streetname (testAddressParse.testAddressParse) ... ok
test_splitted_streetname_with_house_number_extension (testAddressParse.testAddressParse) ... ok
test_splitted_streetname_with_house_number_whitespace_extension (testAddressParse.testAddressParse) ... ok

----------------------------------------------------------------------
Ran 11 tests in 0.002s

OK
```